int nrOfRows=20, halfRows=10;
int nrOfCols=36, halfCols=18;
int nrOfDots=nrOfRows*nrOfCols;
int dotSpace=20;
int dotSize=9;
int i, c, r;

Dot[] d=new Dot[nrOfDots+1];

void setup() {
  size(700,300,P3D);
  background(192);
  lights();
  ellipseMode(RADIUS);
  stroke(0); fill(128);
  for (r=1;r<=nrOfRows;r++) {
    for (c=1;c<=nrOfCols;c++) {
      i=(r-1)*nrOfCols+c;
      d[i]=new Dot(i);
      d[i].init(c,r);
    }
  }
}

void draw() {
  background(192);
  translate(350,150,-150);
  rotateX(HALF_PI/2);
  for (i=1;i<=nrOfDots;i++) {
    d[i].updatePosition();
  }
  for (i=1;i<=nrOfDots;i++) {
    d[i].updateRotation();
    d[i].updateScreen();
  }
}

class Dot {
  int nr; // instance number
  int col, row; // row/column number
  float targetFillColor=128, fillColor=128;
  float dx,dy,dz,distance;
  float pressure;
  float x, y, z=0;
  float x1,x2,y1,y2,z1,z2,yrot=0.0, xrot=0.0;
  float zmov=0.0;
  boolean active=false;
  
  Dot (int n) {nr=n;}
  
  void init(int c,int r) {
    col=c; row=r;
  }
  
  void updatePosition() {
    // check for mouseover:
    // --------------------
    dx=mouseX-screenX(x,y,z);
    dy=mouseY-screenY(x,y,z);
    distance=sqrt(dx*dx+dy*dy);
    
    // change outline and give push on moment of mouseover:
    // ----------------------------------------------------
    if (mousePressed) {
      if (distance<dotSpace*2) {
        pressure = (dotSpace*2-distance)/(dotSpace*2);  // 
        zmov-=pressure*4.0;
        fillColor=128-pressure*64; targetFillColor=fillColor; 
      } else {
        targetFillColor=128;
      }
    }
    fillColor=(7*fillColor+targetFillColor)/8;
    
    // "listen" to neighbour zmov
    // --------------------------
    if (row>1) {zmov+=d[(row-2)*nrOfCols+col].zmov*0.015;}
    if (row<nrOfRows) {zmov+=d[(row)*nrOfCols+col].zmov*0.015;}
    if (col>1) {zmov+=d[(row-1)*nrOfCols+(col-1)].zmov*0.015;}
    if (col<nrOfCols) {zmov+=d[(row-1)*nrOfCols+(col+1)].zmov*0.015;}
    
    // create new position:
    // --------------------
    zmov*=0.94;  // damping;
    zmov+=-z/12; // elasticity;
    z+=zmov;
    x=dotSpace*(col-0.5-halfCols);
    y=dotSpace*(row-0.5-halfRows);
  }
  
  void updateRotation() {
    if (col==1) {x1=x;z1=z;} else {
      x1=d[(row-1)*nrOfCols+col-1].x;
      z1=d[(row-1)*nrOfCols+col-1].z;
    }
    if (col==nrOfCols) {x2=x;z2=z;} else {
      x2=d[(row-1)*nrOfCols+col+1].x;
      z2=d[(row-1)*nrOfCols+col+1].z;
    }
    dx=x2-x1; dz=z2-z1;
    yrot=atan2(dz,dx);
    
    if (row==1) {y1=1;z1=z;} else {
      y1=d[(row-2)*nrOfCols+col].y;
      z1=d[(row-2)*nrOfCols+col].z;
    }
    if (row==nrOfRows) {y2=y;z2=z;} else {
      y2=d[(row)*nrOfCols+col].y;
      z2=d[(row)*nrOfCols+col].z;
    }
    dy=y2-y1; dz=z2-z1;
    xrot=atan2(dz,dy);
  }
  
  void updateScreen() {
    // draw dot on screen:
    // -------------------
    pushMatrix();
    fill(fillColor);
    translate(0,0,z);
    translate(x,y);
    rotateY(-yrot);
    rotateX(xrot);
    ellipse(0,0,dotSize,dotSize);
    popMatrix();
  }
}
