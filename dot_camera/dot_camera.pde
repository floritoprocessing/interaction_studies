int dot_total = 20000;
dot [] dot_array = new dot [dot_total];

PImage bg;
int bgW, bgH, bgSx, bgSy;
int black_array [][] = new int [600*400][3];

void setup(){
  size(600,400);
  background(255);
  bg_setting();
  bg_array_setting();
  init_dot();
}

void bg_setting(){
  bg = loadImage("camera.jpg");
  bgW = bg.width;
  bgH = bg.height;
  bgSx = (width-bgW)/2;
  bgSy = (height-bgH)/2;
}

int k = 0;
void bg_array_setting(){
  bg.loadPixels();
  for(int i=0;i<bgW*bgH; i++){
    if(brightness(bg.pixels[i])>50){
      black_array[k][0] = i%bgW + bgSx;
      black_array[k][1] = int(i/bgW) + bgSy;
      black_array[k][2] = bg.pixels[i];
      k++;
    }
  }
}

void init_dot(){
  for(int i=0; i<dot_total; i++){    
    int rand = (int)random(k);    
    dot_array[i] = new dot();
    dot_array[i].ox = black_array[rand][0];
    dot_array[i].oy = black_array[rand][1];
    dot_array[i].tx = dot_array[i].ox;
    dot_array[i].ty = dot_array[i].oy;
    dot_array[i].x = dot_array[i].ox;
    dot_array[i].y = dot_array[i].oy;
    dot_array[i].col = black_array[rand][2];
  }
}

void draw(){
  background(255);
  for(int i=0;i<dot_total; i++){
    dot_array[i].move();
  }
}

class dot{
  float ox,oy,tx,ty,x,y;
  float dx,dy,dist;
  float mdx,mdy,mdist;
  float mx,my;
  float damperX,damperY;
  color col;
  dot(){
    damperX = 5 + random(15);
    damperY = 5 + random(15);
    col = color(0,0,0);    
  }
  void move(){
    //calculate distance from mouse cursor
    mdx = mouseX - x;
    mdy = mouseY - y;
    mdist = sqrt(sq(mdx)+sq(mdy));
    if(mdist<(20+random(20))){
      // if mouse within 30+-10 pixels away
      mx = mouseX - pmouseX;
      my = mouseY - pmouseY;
      tx = x + mx * (40-mdist)/10 + random(60)-30;
      ty = y + my * (40-mdist)/10 + random(60)-30;
      
      //change ox,oy randomly
      if(random(10)<1){
        int rand = (int)random(k);
        ox = black_array[rand][0];
        oy = black_array[rand][1];
        col = black_array[rand][2];
      }
      
    } else if (abs(tx-x)<1+random(10) && abs(ty-y)<1+random(10)){
      tx = ox + random(20) - 10;
      ty = oy + random(20) - 10;
    }
    
    dx = tx-x;
    dy = ty-y;
//    dist = sqrt(sq(dx)+sq(dy));
    x += dx/damperX;
    y += dy/damperY;
    
    draw_me();
  }
  void draw_me(){
    set(int(x),int(y),col);
//    noStroke();
//    fill(col);
//    rect(x,y,1,1);
  }
}
