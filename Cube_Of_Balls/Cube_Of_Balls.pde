int sideLength=5;
int nrOfBalls=sideLength*sideLength*sideLength;
int ballSize=10;
int spaceSize=50; // 30
float xdeg,ydeg, xdeg_acc;
Balls[] ball=new Balls[2*nrOfBalls+3];

void setup() {
  size(400,400,P3D);
  lights();
  ellipseMode(RADIUS);
  sphereDetail(6);
  int i=1;
  for (int x=0;x<=sideLength-1;x++) {
    for (int y=0;y<=sideLength-1;y++) {
      for (int z=0;z<=sideLength-1;z++) {
        float xp=0.5*(sideLength-1.0)*spaceSize*(2*x/(sideLength-1.0)-1.0);
        float yp=0.5*(sideLength-1.0)*spaceSize*(2*y/(sideLength-1.0)-1.0);
        float zp=0.5*(sideLength-1.0)*spaceSize*(2*z/(sideLength-1.0)-1.0);
        ball[i]=new Balls(xp,yp,zp,i);
        i++;
      }
    }
  }
  xdeg=0.5; xdeg_acc=0.0; ydeg=0.0;
}

void draw() {
  background(0);
  translate(width/2,height/2);
  
  //ydeg=0.2*sin(0.2*TWO_PI*millis()/1000.0); rotateY(ydeg);
  int mx=int(constrain(mouseX,0.25*width,0.75*width));
  
  float t_ydeg=-0.2*TWO_PI * (mx/(float)width-0.5); ydeg=(7*ydeg+t_ydeg)/8.0;
  rotateY(ydeg);
  
  int my=int(constrain(mouseY,0.25*height,0.75*height));
  float t_xdeg_acc=0.16*(my/(float)height-0.5); xdeg_acc=(7*xdeg_acc+t_xdeg_acc)/8.0;
  xdeg+=xdeg_acc;
  rotateX(xdeg);
  
  for (int i=1;i<=nrOfBalls;i++) {
    ball[i].update();
  }
}

void drawBall(float x,float y, float z, int bp, float scl) {
  if (bp<=2) {
    if (bp==1) {
      stroke(128,128,128,64);noFill();
    } else {
      noStroke();fill(255,0,0,128);
    }
    pushMatrix();
    translate(x,y,z);
    sphere(ballSize*scl);
    popMatrix();
  } else if (bp==3) {
    point(x,y,z);
  }
}

class Balls {
  float x,y,z;
  float tScl=1;
  float scl=1;
  int nr;
  Balls(float x, float y, float z,int n) {
    nr=n;
    this.x=x; this.y=y; this.z=z;
  }
  void update() {
    scl=(7*scl+tScl)/8.0;
    if (dist(screenX(x,y,z),screenY(x,y,z),mouseX,mouseY)<ballSize) {
      tScl=3;
      drawBall(x,y,z,2,scl);
    } else {
      tScl=1;
      drawBall(x,y,z,1,scl);
    }
  }
}
